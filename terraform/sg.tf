resource "aws_security_group" "vprofile-ELB-SG" {
  name        = "vprofile-ELB-SG"
  description = "Security group for vprofile prod Load Balancer"
  vpc_id      = aws_vpc.vprofile.id

  ingress {
    description = "Rules for APP from VPC"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Rules for APP from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Vprofile-ELB-SG"
  }
}

resource "aws_security_group" "vprofile-app-sg" {
  name        = "vprofile-app-sg"
  description = "Allow traffic from Vprofile prod ELB"
  vpc_id      = aws_vpc.vprofile.id

  ingress {
    description     = "Rules for APP from ELB-SG"
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    security_groups = ["${aws_security_group.vprofile-ELB-SG.id}"]
  }
  ingress {
    description = "Rules for APP from ELB-SG"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["46.150.64.96/32"]

  }
  ingress {
    description = "Rules for APP from ELB-SG"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["46.150.64.96/32"]

  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Vprofile-APP-SG"
  }
}

resource "aws_security_group" "vprofile-backend-SG" {
  name        = "vprofile-backend-SG"
  description = "Allow Group for Vprofile Backend Services"
  vpc_id      = aws_vpc.vprofile.id

  ingress {
    description     = "Allow 3306 from application servers"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = ["${aws_security_group.vprofile-app-sg.id}"]
  }

  ingress {
    description     = "Allow tomcat to connect memcashe"
    from_port       = 11211
    to_port         = 11211
    protocol        = "tcp"
    security_groups = ["${aws_security_group.vprofile-app-sg.id}"]
  }
  ingress {
    description     = "Allow tomcat to connect RabbitMQ"
    from_port       = 5672
    to_port         = 5672
    protocol        = "tcp"
    security_groups = ["${aws_security_group.vprofile-app-sg.id}"]
  }
  ingress {
    description = "Allow internal traffic to flow on all ports"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
  }
  ingress {
    description = "Rules for APP from ELB-SG"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["46.150.64.96/32"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Vprofile-backend-SG"
  }
}

resource "aws_security_group" "ansible" {
  name        = "Ansible"
  description = "Security group for Ansible access"
  vpc_id      = aws_vpc.vprofile.id

  # Define inbound rules
  ingress {
    description = "SSH access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  # Allow SSH access from anywhere
  }

  # Define outbound rules
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"            # Allow all outbound traffic
    cidr_blocks = ["0.0.0.0/0"]   # Allow outbound traffic to anywhere
  }

  # Add tags to the security group
  tags = {
    Name = "Ansible"
  }
}
