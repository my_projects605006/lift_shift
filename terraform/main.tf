provider "aws" {
  region = "us-east-1"
}

resource "aws_vpc" "vprofile" {
  cidr_block = "10.0.0.0/16"
  tags = {
    Name = "Vprofile"
  }
}

resource "aws_subnet" "front-end-net" {
  vpc_id     = aws_vpc.vprofile.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Public-net"
  }
}

resource "aws_subnet" "front-end-net2" {
  vpc_id     = aws_vpc.vprofile.id
  cidr_block = "10.0.3.0/24"

  tags = {
    Name = "Public-net2"
  }
}

resource "aws_subnet" "back-end-net" {
  vpc_id     = aws_vpc.vprofile.id
  cidr_block = "10.0.2.0/24"

  tags = {
    Name = "Private-net"
  }
}

resource "aws_internet_gateway" "vprofile" {
  vpc_id = aws_vpc.vprofile.id

  tags = {
    Name = "Vprofile"
  }
}

resource "aws_route_table" "vprofile" {
  vpc_id = aws_vpc.vprofile.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.vprofile.id
  }

  tags = {
    Name = "Vprofile-RT-Front"
  }
}

resource "aws_route_table_association" "a-front-net" {
  subnet_id      = aws_subnet.front-end-net.id
  route_table_id = aws_route_table.vprofile.id
}

resource "aws_route_table_association" "a-front-net2" {
  subnet_id      = aws_subnet.front-end-net2.id
  route_table_id = aws_route_table.vprofile.id
}





# output "ec2_public_ip" {
#   value = aws_instance.web-server
# }

# resource "aws_instance" "db-server" {
#   ami           = data.aws_ami.ubuntu-latest.id
#   instance_type = "t2.micro"
#   subnet_id = aws_subnet.back-end-net.id
#   vpc_security_group_ids = [aws_security_group.actpro-sg-db.id]
#   associate_public_ip_address = false

#   key_name = "aws"


#   tags = {
#     Name = "db-server"
#   }
# }















