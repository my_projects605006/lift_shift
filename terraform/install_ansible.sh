#!/bin/bash

# Update package lists
sudo apt update

# Install prerequisite packages
sudo apt install -y software-properties-common

# Add Ansible repository
sudo add-apt-repository --yes --update ppa:ansible/ansible

# Install Ansible
sudo apt install -y ansible

# Verify Ansible installation
ansible --version
