resource "aws_instance" "vprofile-db01" {
  ami                         = "ami-0e731c8a588258d0d"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  security_groups             = [aws_security_group.vprofile-backend-SG.id]
  associate_public_ip_address = true
  key_name                    = "vprofile"

  tags = {
    Name    = "Vprofile-db01"
    Project = "vprofile"
  }

  lifecycle {
    prevent_destroy = false
  }
}

resource "aws_instance" "ansible" {
  ami                         = "ami-0c7217cdde317cfec"
  instance_type               = "t2.micro"
  subnet_id                   = aws_subnet.front-end-net.id
  security_groups             = [aws_security_group.ansible.id]
  associate_public_ip_address = true
  key_name                    = "vprofile"

  tags = {
    Name    = "Ansible"
    Project = "vprofile"
  }

  user_data = <<-EOF
              #!/bin/bash
              sudo apt update
              sudo apt install software-properties-common -y
              sudo apt-add-repository --yes --update ppa:ansible/ansible
              sudo apt install ansible -y
              EOF

  lifecycle {
    prevent_destroy = false
  }
}
